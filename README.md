# sepps_tuple

A tuple implementation which constructs tuples using move where possible.
This implementation works with copy and move-only types.

With this example, this demonstrates the advantage over std::tuple, this advantage becomes more pronounced the move movable types you use.
```CPP
std::cout << "nested inplace, copy, copy, nested inplace:" << "\n";
auto foo1 = Foo<1>{};
auto foo2 = Foo<2>{};
tuple<Foo<0>, Foo<1>, Foo<2>, Foo<3>> vals
{
    { 1, {2, 3, 4}, 5 },
    foo1,
    foo2,
    { 4, {3, 4, 5}, 6 }
};
auto v1 = vals;
auto v2 = std::move(vals);
std::cout << "\n";
```
vs
```CPP
std::cout << "std::tuple nested inplace, copy, copy, nested inplace:" << "\n";
auto foo1 = Foo<1>{};
auto foo2 = Foo<2>{};
std::tuple<Foo<0>, Foo<1>, Foo<2>, Foo<3>> vals
{
    { 1, {2, 3, 4}, 5 },
    foo1,
    foo2,
    { 4, {3, 4, 5}, 6 }
};
auto v1 = vals;
auto v2 = std::move(vals);
std::cout << "\n";
```

The resulting diff:
![Move/Copy Construction Diff](sep_tuple_advantage.png)
