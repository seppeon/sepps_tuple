#include "sep_tuple.hpp"
#include <tuple>
#include <iostream>

template<size_t N>
struct Foo
{
	Foo() noexcept
	{
		std::cout << this << " " << "ctor default " << __func__ << "\n";
	}

	Foo(int, int, int) noexcept
	{
		std::cout << this << " " << "ctor inplace " << __func__ << "\n";
	}

	Foo(int, Foo, int) noexcept
	{
		std::cout << this << " " << "ctor nested inplace " << __func__ << "\n";
	}

	Foo(Foo && foo) noexcept
	{
		std::cout << this << " " << "ctor move " << __func__ << "\n";
	}

	Foo(Foo const & foo) noexcept
	{
		std::cout << this << " " << "ctor copy " << __func__ << "\n";
	}

	Foo& operator=(Foo && foo) noexcept
	{
		std::cout << this << " " << "assign move " << __func__ << "\n";
		return *this;
	}

	Foo& operator=(Foo const & foo) noexcept
	{
		std::cout << this << " " << "assign copy " << __func__ << "\n";
		return *this;
	}

	~Foo()
	{
		std::cout << this << " " << "dtor " << __func__ << "\n";
	}
};

struct Mova
{
	Mova& operator=(Mova const &) = delete;
	Mova(Mova const &) = delete;

	Mova& operator=(Mova &&) = default;
	Mova(Mova &&) = default;

	Mova() = default;
};

struct Copya
{
	Copya& operator=(Copya const &) = default;
	Copya(Copya const &) = default;

	Copya& operator=(Copya &&) = delete;
	Copya(Copya &&) = delete;

	Copya() = default;
};

int main()
{
	{
		int temp = 20;
		auto f = std::tuple_cat(std::tuple{10, 20, temp}, std::tie(temp));
		auto [a, b, c, d] = f;
		d = 30;
		std::cout << temp << "\n";
	}
	{
		int temp = 20;
		auto e = sep::tuple_cat(sep::tuple{10, 20, temp}, sep::tie(temp));
		auto [a, b, c, d] = e;
		d = 30;
		std::cout << temp << "\n";
	}
	{
		int temp = 20;
		auto f = sep::forward_as_tuple(10, 20, temp); 
		auto & [a, b, c] = f;
		c = 10;
		std::cout << temp << "\n";
	}
	{
		int a;
		auto [ ref ] = sep::tie( a );
		ref = 10;
		std::cout << a << "\n";
	}
	{
		int a;
		auto [ ref ] = std::tie( a );
		ref = 10;
		std::cout << a << "\n";
	}
	{
		std::cout << "tuple Default construct:" << "\n";
		sep::tuple<Foo<0>, Foo<1>, Foo<2>, Foo<3>> vals;
		std::cout << "\n";
	}
	{
		std::cout << "std::tuple Default construct:" << "\n";
		std::tuple<Foo<0>, Foo<1>, Foo<2>, Foo<3>> vals;
		std::cout << "\n";
	}
	{
		std::cout << "Move only:" << "\n";
		sep::tuple vals
		{
			Foo<0>{},
			Foo<1>{},
			Foo<2>{},
			Foo<3>{},
		};

		using terminal_init = typename decltype(vals)::bl;

		auto v1 = vals;
		auto v2 = std::move(vals);

		swap(v1, v2);
		std::cout << "\n";
	}
	{
		std::cout << "move, copy, copy, move:" << "\n";
		auto foo1 = Foo<1>{};
		auto foo2 = Foo<2>{};
		sep::tuple vals
		{
			Foo<0>{},
			foo1,
			foo2,
			Foo<3>{}
		};
		using terminal_init = typename decltype(vals)::bl;

		auto v1 = vals;
		auto v2 = std::move(vals);
		std::cout << "\n";
	}
	{
		std::cout << "inplace, copy, copy, inplace:" << "\n";
		auto foo1 = Foo<1>{};
		auto foo2 = Foo<2>{};
		sep::tuple<Foo<0>, Foo<1>, Foo<2>, Foo<3>> vals
		{
			{ 1, 2, 3 },
			foo1,
			foo2,
			{ 4, 5, 6 }
		};
		auto v1 = vals;
		auto v2 = std::move(vals);
		std::cout << "\n";
	}
	{
		std::cout << "tuple nested inplace, copy, copy, nested inplace:" << "\n";
		auto foo1 = Foo<1>{};
		auto foo2 = Foo<2>{};
		sep::tuple<Foo<0>, Foo<1>, Foo<2>, Foo<3>> vals
		{
			{ 1, {2, 3, 4}, 5 },
			foo1,
			foo2,
			{ 4, {3, 4, 5}, 6 }
		};
		auto v1 = vals;
		auto v2 = std::move(vals);
		std::cout << "\n";
	}
	{
		std::cout << "tuple nested inplace, copy, nested inplace, nested inplace:" << "\n";
		auto foo1 = Foo<1>{};
		auto foo2 = Foo<2>{};
		sep::tuple<Foo<0>, Copya, Foo<2>, Foo<3>> vals
		{
			{ 1, 2, 3 },
			{},
			{ 1, 2, 3 },
			{ 1, 2, 3 },
		};
		auto v1 = vals;
		auto v2 = std::move(vals);
		std::cout << "\n";
	}
	{
		std::cout << "tuple nested inplace, move, nested inplace, nested inplace:" << "\n";
		auto foo1 = Foo<1>{};
		auto foo2 = Foo<2>{};
		sep::tuple<Foo<0>, Mova, Foo<2>, Foo<3>> vals
		{
			{ 1, 2, 3 },
			{},
			{ 1, 2, 3 },
			{ 1, 2, 3 },
		};
		auto v2 = std::move(vals);
		std::cout << "\n";
	}
    // // std::tuple cannot do this:
	// {
	// 	std::cout << "std::tuple nested inplace, copy, nested inplace, nested inplace:" << "\n";
	// 	auto foo1 = Foo<1>{};
	// 	auto foo2 = Foo<2>{};
	// 	std::tuple<Foo<0>, Copya, Foo<2>, Foo<3>> vals
	// 	{
	// 		{ 1, 2, 3 },
	// 		{},
	// 		{ 1, 2, 3 },
	// 		{ 1, 2, 3 },
	// 	};
	// 	auto v1 = vals;
	// 	auto v2 = std::move(vals);
	// 	std::cout << "\n";
	// }
    // // std::tuple cannot do this:
	// {
	// 	std::cout << "std::tuple nested inplace, move, nested inplace, nested inplace:" << "\n";
	// 	auto foo1 = Foo<1>{};
	// 	auto foo2 = Foo<2>{};
	// 	std::tuple<Foo<0>, Mova, Foo<2>, Foo<3>> vals
	// 	{
	// 		{ 1, 2, 3 },
	// 		{},
	// 		{ 1, 2, 3 },
	// 		{ 1, 2, 3 },
	// 	};
	// 	auto v1 = vals;
	// 	auto v2 = std::move(vals);
	// 	std::cout << "\n";
	// }
}
